﻿using System;

namespace GradeBook
{
    class Program
    {
        static void Main(string[] args)
        {
            if(args.Length > 0)
            {
                // $ at the beginning of the string allows string interpolation
                Console.WriteLine($"Hello, {args[0]}!");
            }
            else {
                Console.WriteLine("Hello mystery person. You didn't pass a name argument. I don't know who you are!");
            }
        }
    }
}
